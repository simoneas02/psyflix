import React from 'react';
import './App.css';
import Menu from '../Menu';
import BannerrMain from '../BannerMain';
import Carousel from '../Carousel';

import content from '../../assets/data/content.json';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Menu />
      </header>

      <BannerrMain
        videoTitle={content.categories[0].videos[0].title}
        url={content.categories[0].videos[0].url}
        valorDescription={'O Melhor psy trance'}
      />

      <Carousel ignoreFirstVideo category={content.categories[0]} />
    </div>
  );
}

export default App;

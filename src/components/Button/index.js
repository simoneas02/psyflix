import styled from 'styled-components';

const Button = styled.button`
  text-decoration: none;
  cursor: pointer;
  padding: 16px 24px;
  border-radius: 5px;
  display: inline-block;
  font-size: 16px;
  color: whitesmoke;
  border: 1px solid whitesmoke;
  transition: opacity 3s;

  &:hover,
  & :focus {
    opacity: 0.5;
  }
`;

export default Button;

import React from 'react';

const ButtonLink = ({ href, children }) => <a href={href}>{children}</a>;

export default ButtonLink;

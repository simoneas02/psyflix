import React from 'react';
import logo from '../../assets/imgs/logo.png';
import Button from '../Button';

const Menu = () => (
  <nav>
    <a href="/">
      <img src={logo} alt="Letreiro com a palavra Psyflix" />
    </a>
    <Button as="a" href="/">
      Home
    </Button>
  </nav>
);

export default Menu;
